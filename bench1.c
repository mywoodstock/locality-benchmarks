/***
  *  Project:
  *
  *  File: bench1.c
  *  Created: Feb 22, 2014
  *  Modified: Mon 24 Feb 2014 03:27:17 PM PST
  *
  *  Author: Abhinav Sarje <asarje@lbl.gov>
  */

#include <upc.h>
#include <stdio.h>

#include "woo_bupctimers.h"

const int N1 = 100;	// fastest dimension
const int N2 = 100;
const int N3 = 100;
const int N4 = 512;	// outermost dimension

const int NTRIALS = 10;


void print_results(UPCTimer timer, unsigned long int size) {
	upc_barrier;
	double time = upctimer_elapsed_msec(&timer);
	printf("Thread %d:\t[ %.3f ms ]\t\t%.2f MB/s\n", MYTHREAD, time / NTRIALS,
			NTRIALS * ((double) size / 1024 / 1024) * 1000 / time);
	upc_barrier;
} // print_results()


// with 1D decomposition along N4
int bench1(unsigned int n1, unsigned int n2, unsigned int n3, unsigned int n4) {

	unsigned int total_size = n1 * n2 * n3;
	unsigned int local_n4 = ceil(((float) n4) / THREADS);
	unsigned long int local_data_size = total_size * local_n4 * sizeof(double);	// in bytes

	if(MYTHREAD == 0) {
		printf("\n**      TOTAL MATRIX SIZE: %dx%dx%dx%d = %ld\n", n1, n2, n3, n4, total_size * n4);
		printf("** WORKING SET PER THREAD: %ld MB\n", local_data_size / 1024 / 1024);
		printf("**      NUMBER OF THREADS: %d\n\n", THREADS);
	} // if

	shared [4] double *inp_arr = (shared [4] double*) upc_all_alloc(THREADS, local_data_size);

	upc_barrier;

	upc_forall(int i = 0; i < total_size * n4; ++ i; &inp_arr[i]) inp_arr[i] = i;

	// access array in different ways
	UPCTimer lkji, likj, ljik, ljki, lijk, lkij;
	double local_sum = 0.0;

	upc_barrier;
	if(MYTHREAD == 0) printf("\n** LKJI *****\n\n");
	local_sum = 0.0;
	upctimer_start(&lkji);
	for(int t = 0; t < NTRIALS; ++ t) {
//	upc_barrier;
	for(int l = 0; l < local_n4; ++ l) {
		for(int k = 0; k < n3; ++ k) {
			for(int j = 0; j < n2; ++ j) {
				for(int i = 0; i < n1; ++ i) {
					unsigned int index = MYTHREAD * local_n4 * n3 * n2 * n1 + l * n3 * n2 * n1 +
											k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	} // for t
	upctimer_stop(&lkji);
	print_results(lkji, local_data_size);
//	printf("SUM%d: %lf\t[ %lf ms ]\n", MYTHREAD, local_sum, upctimer_elapsed_msec(&lkji) / NTRIALS);

	upc_barrier;
	if(MYTHREAD == 0) printf("\n** LIKJ *****\n\n");
	local_sum = 0.0;
	upctimer_start(&likj);
	for(int t = 0; t < NTRIALS; ++ t) {
//	upc_barrier;
	for(int l = 0; l < local_n4; ++ l) {
		for(int i = 0; i < n1; ++ i) {
			for(int k = 0; k < n3; ++ k) {
				for(int j = 0; j < n2; ++ j) {
					unsigned int index = MYTHREAD * local_n4 * n3 * n2 * n1 + l * n3 * n2 * n1 +
											k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	} // for t
	upctimer_stop(&likj);
	print_results(likj, local_data_size);
//	printf("SUM%d: %lf\t[ %lf ms ]\n", MYTHREAD, local_sum, upctimer_elapsed_msec(&likj) / NTRIALS);

	upc_barrier;
	if(MYTHREAD == 0) printf("\n** LJIK *****\n\n");
	local_sum = 0.0;
	upctimer_start(&ljik);
	for(int t = 0; t < NTRIALS; ++ t) {
//	upc_barrier;
	for(int l = 0; l < local_n4; ++ l) {
		for(int j = 0; j < n2; ++ j) {
			for(int i = 0; i < n1; ++ i) {
				for(int k = 0; k < n3; ++ k) {
					unsigned int index = MYTHREAD * local_n4 * n3 * n2 * n1 + l * n3 * n2 * n1 +
											k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	} // for t
	upctimer_stop(&ljik);
	print_results(ljik, local_data_size);
//	printf("SUM%d: %lf\t[ %lf ms ]\n", MYTHREAD, local_sum, upctimer_elapsed_msec(&ljik) / NTRIALS);

	upc_barrier;
	if(MYTHREAD == 0) printf("\n** LJKI *****\n\n");
	local_sum = 0.0;
	upctimer_start(&ljki);
	for(int t = 0; t < NTRIALS; ++ t) {
//	upc_barrier;
	for(int l = 0; l < local_n4; ++ l) {
		for(int j = 0; j < n2; ++ j) {
			for(int k = 0; k < n3; ++ k) {
				for(int i = 0; i < n1; ++ i) {
					unsigned int index = MYTHREAD * local_n4 * n3 * n2 * n1 + l * n3 * n2 * n1 +
											k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	} // for t
	upctimer_stop(&ljki);
	print_results(ljki, local_data_size);
//	printf("SUM%d: %lf\t[ %lf ms ]\n", MYTHREAD, local_sum, upctimer_elapsed_msec(&ljki) / NTRIALS);

	upc_barrier;
	if(MYTHREAD == 0) printf("\n** LIJK *****\n\n");
	local_sum = 0.0;
	upctimer_start(&lijk);
	for(int t = 0; t < NTRIALS; ++ t) {
//	upc_barrier;
	for(int l = 0; l < local_n4; ++ l) {
		for(int i = 0; i < n1; ++ i) {
			for(int j = 0; j < n2; ++ j) {
				for(int k = 0; k < n3; ++ k) {
					unsigned int index = MYTHREAD * local_n4 * n3 * n2 * n1 + l * n3 * n2 * n1 +
											k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	} // for t
	upctimer_stop(&lijk);
	print_results(lijk, local_data_size);
//	printf("SUM%d: %lf\t[ %lf ms ]\n", MYTHREAD, local_sum, upctimer_elapsed_msec(&lijk) / NTRIALS);

	upc_barrier;
	if(MYTHREAD == 0) printf("\n** LKIJ *****\n\n");
	local_sum = 0.0;
	upctimer_start(&lkij);
	for(int t = 0; t < NTRIALS; ++ t) {
//	upc_barrier;
	for(int l = 0; l < local_n4; ++ l) {
		for(int k = 0; k < n3; ++ k) {
			for(int i = 0; i < n1; ++ i) {
				for(int j = 0; j < n2; ++ j) {
					unsigned int index = MYTHREAD * local_n4 * n3 * n2 * n1 + l * n3 * n2 * n1 +
											k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	} // for t
	upctimer_stop(&lkij);
	print_results(lkij, local_data_size);
//	printf("SUM%d: %lf\t[ %lf ms ]\n", MYTHREAD, local_sum, upctimer_elapsed_msec(&lkij) / NTRIALS);

	upc_all_free(inp_arr);
	return 0;
} // bench1()

int main(int narg, char** args) {
	unsigned int n1 = N1, n2 = N2, n3 = N3, n4 = N4;
	if(narg == 5) {
		n1 = atoi(args[1]);
		n2 = atoi(args[2]);
		n3 = atoi(args[3]);
		n4 = atoi(args[4]);
	} // if
	bench1(n1, n2, n3, n4);

	return 0;
} // main()
