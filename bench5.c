/***
  *  Project: Locality - multiple masters (request fulfillers)
  *
  *  File: bench4.c
  *  Created: Feb 22, 2014
  *  Modified: Fri 04 Apr 2014 08:26:00 PM PDT
  *
  *  Author: Abhinav Sarje <asarje@lbl.gov>
  */

#include <stdio.h>
#include <upc.h>
#include <upc_collective.h>
#include <upc_nb.h>
#include <bupc_atomics.h>
#include <upc_castable.h>

#include "woo_bupctimers.h"

#define MIN(a, b) (a > b ? b : a)

const int N1 = 100;	// fastest dimension
const int N2 = 100;
const int N3 = 100;
const int N4 = 512;	// outermost dimension

const int NTRIALS = 5;

// masks
uint64_t MMASKS[8] = {	0x00000000000000FF,
						0x000000000000FF00,
						0x0000000000FF0000,
						0x00000000FF000000,
						0x000000FF00000000,
						0x0000FF0000000000,
						0x00FF000000000000,
						0xFF00000000000000	};
uint64_t WMASKS[8] = {	0x0101010101010101,
						0x0202020202020202,
						0x0404040404040404,
						0x0808080808080808,
						0x1010101010101010,
						0x2020202020202020,
						0x4040404040404040,
						0x8080808080808080	};


typedef shared [] double * shared_double_t;
typedef shared [1] double * shared_1_double_t;
typedef struct {
	unsigned int src_offset;
	unsigned int lengths;
	unsigned int strides;
	unsigned int dst_thread;
	unsigned int dst_offset;
} upc_request_t;


shared uint64_t req_avail;						// flag	for worker -> master
shared upc_request_t requests[THREADS];			// request from each thread
shared unsigned int req_done[THREADS];			// flag for master -> worker


// for checking correctness
shared double totsum;
double check_local_data(double* arr, unsigned int size) {
	double mysum = 0.0;
	for(int i = 0; i < size; ++ i) mysum += arr[i];

	shared double * sums = (shared double *) upc_all_alloc(THREADS, sizeof(double));
	sums[MYTHREAD] = mysum;

	upc_all_reduceD(&totsum, sums, UPC_ADD, THREADS, 1, NULL, 0);
	mysum = totsum;

	upc_all_free(sums);
	return mysum / size;
} // check_local_data()


void print_perf(UPCTimer timer, double size) {		// size is in MB over NTRIALS
	printf("  ++ Thread %d:        Data moved = %.0f MB\n", MYTHREAD, size / NTRIALS);
	double time = upctimer_elapsed_msec(&timer);
	printf("  ++ Thread %d:      Elapsed time = %.3f ms\t[ %.2f MB/s ]\n", MYTHREAD, time / NTRIALS,
			size * 1000 / time);
} // print_results()


// gather and put for fulfiling requests
// gather data into local buffer and then perform single memput/memcpy
void my_gather_and_put(shared_1_double_t base, int thread_id, int src_off, int lengths, int strides,
						int dst_off, double* local_src, double* local_buff, unsigned int local_size,
						unsigned int* words_moved) {
	*words_moved = 0;
	// ignoring src_off and dst_off
	int buff_off = 0;
	int tot_size = 0;
	int size = lengths * sizeof(double);
	// first gather all local data into local buffer
	src_off = 0;
	while(src_off < MYTHREAD * local_size) src_off += (lengths + strides);
	// case when local starts with new segment
	src_off -= MYTHREAD * local_size;	// make it local
	while(src_off < local_size) {
		size = MIN(size, (local_size - src_off) * sizeof(double));
		memcpy(local_buff + buff_off, local_src + src_off, size);
		*words_moved += lengths;
		src_off += (lengths + strides);
		buff_off += lengths;
		tot_size += size;
	} // while
	// and put the gathered data to its destination
	shared_double_t dst = (shared_double_t) (base + thread_id);
	dst_off = (MYTHREAD * local_size / (lengths + strides)) * lengths;
	//if(upc_threadof(dst + dst_off) == MYTHREAD)	// TODO: do this later ...
	//	memcpy((double *) (dst + dst_off), local_buff, tot_size);
	//else
	//printf("%d: tot_size = %d, dst_off = %d\n", MYTHREAD, tot_size / sizeof(double), dst_off);
	if(dst_off + tot_size / sizeof(double) < local_size) {	// whole segment lies with dst thread
		upc_memput(dst + dst_off, local_buff, tot_size);
		*words_moved += tot_size / sizeof(double);
	} else if(dst_off < local_size) {						// partial segment lies with dst thread
		unsigned int done_len = local_size - dst_off;
		upc_memput(dst + dst_off, local_buff, done_len * sizeof(double));
		*words_moved += done_len;
		unsigned int rem_len = tot_size / sizeof(double) - done_len;
		while(rem_len > 0) {
			++ thread_id; dst_off = 0;
			if(thread_id >= THREADS) break;
			dst = (shared_double_t) (base + thread_id);
			unsigned int temp = MIN(rem_len, local_size);
			upc_memput(dst + dst_off, local_buff + done_len, temp * sizeof(double));
			*words_moved += temp;
			rem_len -= temp; done_len += temp;
		} // while
	} else {												// nothing lies with dst thread
		unsigned int rem_len = tot_size / sizeof(double);
		while(dst_off > local_size) { dst_off -= local_size; ++ thread_id; }
		if(thread_id < THREADS) {
			unsigned int done_len = 0;
			while(rem_len > 0) {
				dst = (shared_double_t) (base + thread_id);
				unsigned int temp = MIN(rem_len, local_size - dst_off);
				upc_memput(dst + dst_off, local_buff + done_len, temp * sizeof(double));
				*words_moved += temp;
				rem_len -= temp; done_len += temp; dst_off = 0;
				++ thread_id;
				if(thread_id >= THREADS) break;
			} // while
		} // if
	} // if-else
} // my_gather_and_put()


// perform a memput/memcpy for each segment
void my_gather_and_put2(shared_1_double_t base, int thread_id, int src_off, int lengths, int strides,
						int dst_off, double* local_src, double* local_buff, unsigned int local_size,
						unsigned int* words_moved) {
	*words_moved = 0;
	int buff_off = 0;
	int size = lengths * sizeof(double);
	dst_off = (MYTHREAD * local_size / (lengths + strides)) * lengths;
	src_off = 0;
	while(src_off < MYTHREAD * local_size) src_off += (lengths + strides);
	src_off -= MYTHREAD * local_size;	// make it local
	while(dst_off > local_size) {
		++ thread_id;
		dst_off -= local_size;
	} // while
	if(thread_id >= THREADS) return;
	shared_double_t dst = (shared_double_t) (base + thread_id);
	while(src_off + lengths < local_size && dst_off + lengths < local_size) {
		upc_memput_nbi(dst + dst_off, local_src + src_off, lengths * sizeof(double));
		*words_moved += lengths;
		src_off += (lengths + strides);
		dst_off += lengths;
	} // while
	unsigned int rem = 0;
	if(src_off + lengths < local_size) {			// dst_off is overflowing
		unsigned int len = local_size - dst_off;
		upc_memput_nbi(dst + dst_off, local_src + src_off, len * sizeof(double));
		*words_moved += len;
		src_off += len;
		rem = lengths - len;
		++ thread_id;								// next thread is the new destination
		dst = (shared_double_t) (base + thread_id);
		dst_off = 0;
		upc_memput_nbi(dst + dst_off, local_src + src_off, rem * sizeof(double));
		*words_moved += rem;
		src_off += (rem + strides);
		dst_off += rem;
		// go to while ...
		while(src_off + lengths < local_size && dst_off + lengths < local_size) {
			upc_memput_nbi(dst + dst_off, local_src + src_off, lengths * sizeof(double));
			*words_moved += lengths;
			src_off += (lengths + strides);
			dst_off += lengths;
		} // while
	} else if(src_off < local_size && dst_off + lengths < local_size) {		// src_off is overflowing
		unsigned int len = local_size - src_off;
		upc_memput_nbi(dst + dst_off, local_src + src_off, len * sizeof(double));
		*words_moved += len;
		dst_off += len;
	} else if(src_off < local_size) {										// both are overflowing
		unsigned int len = MIN(local_size - src_off, local_size - dst_off);
		upc_memput_nbi(dst + dst_off, local_src + src_off, len * sizeof(double));
		*words_moved += len;
		dst_off += len;
	} // if-else
	upc_synci();
} // my_gather_and_put2()


void set_request(unsigned int offset, unsigned int length, unsigned int stride,
					upc_request_t* req, unsigned int signal) {
	req->src_offset = 0;			// for now keep offsets at 0
	req->lengths = length;
	req->strides = stride;
	req->dst_thread = MYTHREAD;
	req->dst_offset = 0;			// for now keep offets at 0
	// to notify all the masters
	//bupc_atomicU64_fetchor_relaxed(&req_avail, signal);
	req_avail = req_avail | signal;
} // set_request()


void process_request(shared_1_double_t base, double* local_src, double* local_buff, unsigned int local_size,
						unsigned int tid, UPCTimer* timer, unsigned int* words_moved) {
	// get the request into a local var
	upc_request_t req;
	upc_memget(&req, requests + tid, sizeof(upc_request_t));
	// do the gather and put
	upctimer_resume(timer);
	my_gather_and_put(base, req.dst_thread, 0,
						req.lengths, req.strides, 0,
						local_src, local_buff, local_size, words_moved);
	// to notify worker that its done
	//bupc_atomicUI_fetchor_strict((req_done + tid), (1 << MYTHREAD));
	*(req_done + tid) = *(req_done + tid) | (1 << MYTHREAD);
	upctimer_pause(timer);
} // process_request()


void process_request2(shared_1_double_t base, double* local_src, double* local_buff, unsigned int local_size,
						unsigned int tid, UPCTimer* timer, unsigned int* words_moved) {
	// get the request into a local var
	upc_request_t req;
	upc_memget(&req, requests + tid, sizeof(upc_request_t));
	// do the gather and put
	upctimer_resume(timer);
	my_gather_and_put2(base, req.dst_thread, 0,
						req.lengths, req.strides, 0,
						local_src, local_buff, local_size, words_moved);
	// to notify worker that its done
	//bupc_atomicUI_fetchor_strict((req_done + tid), (1 << MYTHREAD));
	*(req_done + tid) = *(req_done + tid) | (1 << MYTHREAD);
	upctimer_pause(timer);
} // process_request2()


int bench5(unsigned int n1, unsigned int n2, unsigned int n3, unsigned int n4,
			unsigned int length, unsigned int stride) {

	unsigned long int n123 = n1 * n2 * n3;
	unsigned int local_n4 = ceil(((float) n4) / THREADS);
	unsigned long int local_size = n123 * local_n4;
	unsigned long int local_data_size = local_size * sizeof(double);	// in bytes

	if(MYTHREAD == 0) {
		printf("\n**      TOTAL MATRIX SIZE: %dx%dx%dx%d = %ld\n", n1, n2, n3, n4, n123 * n4);
		printf("** WORKING SET PER THREAD: %d MB\n", (int) ((double) local_data_size / 1024 / 1024));
		printf("**      NUMBER OF THREADS: %d\n", THREADS);
	} // if

	shared_1_double_t arr = (shared_1_double_t) upc_all_alloc(THREADS, local_data_size);
	shared_double_t inp_arr = (shared_double_t) (arr + MYTHREAD);

	// local pointers
	double *local_ptr = (double *) (inp_arr);
	unsigned int * req_done_ptr = (unsigned int *) (req_done + MYTHREAD);	// my flag
	upc_request_t * requests_ptr = (upc_request_t *) (requests + MYTHREAD);	// my request

	// allocate and set local buffer
	shared_double_t local_buff = (shared_double_t) upc_alloc(local_data_size);
	double * local_buff_ptr = (double *) local_buff;
	for(int i = 0; i < local_size; ++ i) local_buff_ptr[i] = local_n4 * MYTHREAD + i;
	// and touch your part of the shared buffer
	memcpy(local_ptr, local_buff_ptr, local_data_size);

	if(THREADS > 16)
		if(MYTHREAD == 0)
			printf("warning: this code does not support more than 16 threads yet\n");

	if(MYTHREAD == 0) { req_avail = 0; }	// initialize
	int num_req_per_thread = 20;

	// identify if i am a worker or master
	int num_workers = 1;	// THREADS / 2;
	int num_masters = THREADS / 2;
	int master = 0;
	if(MYTHREAD < num_masters) master = 1;
	else master = 0;
	// identify mask to use
	uint64_t mask = 0;
	if(master) mask = MMASKS[MYTHREAD];
	else mask = WMASKS[MYTHREAD - THREADS / 2];
	// calculate the "done" number
	unsigned int done = 0;
	for(int i = 0; i < num_masters; ++ i) done = 2 * done + 1;

	UPCTimer reqtimer; upctimer_start(&reqtimer); upctimer_pause(&reqtimer);
	UPCTimer req2timer; upctimer_start(&req2timer); upctimer_pause(&req2timer);
	uint64_t type1_words = 0, type2_words = 0;

	if(MYTHREAD == 0) {
		printf("\n** Gather and put: Length = %d, Stride = %d **\n", length, stride);
		printf("** Number of request generators: %d\n", num_workers);
		printf("** Number of request processors: %d\n", THREADS / 2);
	} // if

	// TYPE 1
	if(MYTHREAD == 0) printf("** Type 1:\n");
	for(int t = 0; t < NTRIALS; ++ t) {
		if(MYTHREAD == 0) printf("  -- Trial %d ...\n", t);
		*req_done_ptr = done;
		upc_barrier;

		if(master) { 		// MASTER thread - satisfies requests from other threads
			int num_iter = num_req_per_thread * num_workers;
			for(int i = 0; i < num_iter;) {
				uint64_t tids;
				while((tids = (bupc_atomicU64_mswap_strict(&req_avail, mask, (uint64_t) 0)) & mask) == 0);
				tids = tids >> (MYTHREAD * 8);
				unsigned int tid = THREADS / 2 + num_workers - 1;
				while(tid >= THREADS / 2) {
					// decode tids
					if(tids & (((uint64_t) 1) << tid) == 0) continue;
					unsigned int words = 0;
					process_request(arr, local_ptr, local_buff_ptr, local_size, tid, &reqtimer, &words);
					type1_words += (uint64_t) words;
					-- tid;
					++ i;
				} // while
			} // for
		} else {			// WORKER thread - generates requests for master
			// lets have only one worker for now
			if(MYTHREAD >= THREADS / 2 && MYTHREAD < THREADS / 2 + num_workers) {
				int num_iter = num_req_per_thread;
				for(int i = 0; i < num_iter; ++ i) {
					// wait for your request to be done
					unsigned int hmm = 0;
					while((hmm = bupc_atomicUI_cswap_private(req_done_ptr, done, 0)) != done);
					//while(bupc_atomicUI_cswap_strict(req_done + MYTHREAD, done, 0) != done);
					// and then set request
					set_request(0, length, stride, requests_ptr, mask);
				} // for
			} // if
		} // if-else
	} // for

	// TYPE 2
	if(MYTHREAD == 0) printf("** Type 2:\n");
	for(int t = 0; t < NTRIALS; ++ t) {
		if(MYTHREAD == 0) printf("  -- Trial %d ...\n", t);
		*req_done_ptr = done;
		upc_barrier;

		if(master) { 		// MASTER thread - satisfies requests from other threads
			int num_iter = num_req_per_thread * num_workers;
			for(int i = 0; i < num_iter;) {
				uint64_t tids;
				while((tids = bupc_atomicU64_mswap_strict(&req_avail, mask, (uint64_t) 0) & mask) == 0);
				tids = tids >> (MYTHREAD * 8);
				unsigned int tid = THREADS / 2 + num_workers - 1;
				while(tid >= THREADS / 2) {
					// decode tids
					if(tids & (((uint64_t) 1) << tid) == 0) continue;
					unsigned int words = 0;
					process_request2(arr, local_ptr, local_buff_ptr, local_size, tid, &req2timer, &words);
					type2_words += (uint64_t) words;
					-- tid;
					++ i;
				} // while
			} // for
		} else {			// WORKER thread - generates requests for master
			if(MYTHREAD >= THREADS / 2 && MYTHREAD < THREADS / 2 + num_workers) {
				int num_iter = num_req_per_thread;
				for(int i = 0; i < num_iter; ++ i) {
					// wait for your request to be done
					while(bupc_atomicUI_cswap_private(req_done_ptr, done, 0) != done);
					// and then set request
					set_request(0, length, stride, requests_ptr, mask);
				} // for
			} // if
		} // if-else
	} // for

	upctimer_stop(&reqtimer);
	upctimer_stop(&req2timer);

	if(MYTHREAD == 0) printf("\n++ Gather-and-put performance **\n\n");
	for(int i = 0; i < num_masters; ++ i) {
		if(MYTHREAD == i) {
			printf("++ Type 1:\n");
			print_perf(reqtimer, 2.0 * type1_words * sizeof(double) / 1024 / 1024);
			printf("++ Type 2:\n");
			print_perf(req2timer, 2.0 * type2_words * sizeof(double) / 1024 / 1024);
			printf("\n");
		} // if
		upc_barrier;
	} // if

	upc_free(local_buff);
	upc_all_free(inp_arr);
	return 0;
} // bench5()


int main(int narg, char** args) {
	unsigned int n1 = N1, n2 = N2, n3 = N3, n4 = N4;
	unsigned int length = 1, stride = 1;
	if(narg == 7) {
		n1 = atoi(args[1]);
		n2 = atoi(args[2]);
		n3 = atoi(args[3]);
		n4 = atoi(args[4]);
		length = atoi(args[5]);
		stride = atoi(args[6]);
	} // if
	bench5(n1, n2, n3, n4, length, stride);

	return 0;
} // main()
