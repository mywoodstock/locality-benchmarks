/***
  *  Project:
  *
  *  File: bench1.c
  *  Created: Feb 22, 2014
  *  Modified: Tue 18 Mar 2014 12:55:38 PM PDT
  *
  *  Author: Abhinav Sarje <asarje@lbl.gov>
  */

#include <stdio.h>
#include <upc.h>
#include <upc_collective.h>
#include <upc_nb.h>

#include "woo_bupctimers.h"

const int N1 = 100;	// fastest dimension
const int N2 = 100;
const int N3 = 100;
const int N4 = 512;	// outermost dimension

const int NTRIALS = 10;


typedef shared [] double * shared_double_t;
typedef shared [1] double * shared_1_double_t;

shared int lengths[THREADS];
shared int offsets[THREADS];

typedef struct {
	unsigned int src_offset;
	unsigned int lengths;
	unsigned int strides;
	unsigned int dst_thread;
	unsigned int dst_offset;
} upc_request_t;

volatile shared upc_request_t requests;		// this can be a queue
volatile shared int request_avail;

shared double totsum;
double check_local_data(double* arr, unsigned int size) {
	double mysum = 0.0;
	for(int i = 0; i < size; ++ i) mysum += arr[i];

	shared double * sums = (shared double *) upc_all_alloc(THREADS, sizeof(double));
	sums[MYTHREAD] = mysum;

	upc_all_reduceD(&totsum, sums, UPC_ADD, THREADS, 1, NULL, 0);
	mysum = totsum;

	upc_all_free(sums);
	return mysum / size;
} // check_local_data()


void print_timings(UPCTimer timer, unsigned long int size) {
	double time = upctimer_elapsed_msec(&timer);
	printf("  ++ Thread %d:\t%.3f ms\t\t[ %.2f MB/s ]\n", MYTHREAD, time / NTRIALS,
			NTRIALS * ((double) size / 1024 / 1024) * 1000 / time);
} // print_results()


// synchronous gather
void my_gather(shared_1_double_t base, shared int off[], shared int len[],
				int dest, double * dst) {
	int tot_off = 0;
	if(MYTHREAD == dest) {
		for(int i = 0; i < THREADS; ++ i) {
			shared_double_t src = (shared_double_t) (base + i);
			upc_memget(dst + tot_off, src + off[i], len[i] * sizeof(double));
			tot_off += len[i];
		} // for
	} // if
	upc_fence;
} // my_gather()


// asynchronous gather
void my_gather_async(shared_1_double_t base, shared int off[], shared int len[],
						int dest, double * dst) {
	int tot_off = 0;
	upc_handle_t* handles = (upc_handle_t*) malloc(THREADS * sizeof(upc_handle_t));
	if(MYTHREAD == dest) {
		for(int i = 0; i < THREADS; ++ i) {
			shared_double_t src = (shared_double_t) (base + i);
			handles[i] = upc_memget_nb(dst + tot_off, src + off[i], len[i] * sizeof(double));
			tot_off += len[i];
		} // for
		for(int i = 0; i < THREADS; ++ i) {
			upc_sync(handles[i]);
		} // for
	} // if
	upc_fence;
	free(handles);
} // my_gather()


// asynchronous gather - implicit
void my_gather_asynci(shared_1_double_t base, shared int off[], shared int len[],
						int dest, double * dst) {
	int tot_off = 0;
	if(MYTHREAD == dest) {
		for(int i = 0; i < THREADS; ++ i) {
			shared_double_t src = (shared_double_t) (base + i);
			upc_memget_nbi(dst + tot_off, src + off[i], len[i] * sizeof(double));
			tot_off += len[i];
		} // for
		upc_synci();
	} // if
	upc_fence;
} // my_gather()


// synchronous allgather
void my_all_gather(shared_1_double_t base, shared int off[], shared int len[],
					int dest, double * dst) {
	int tot_off = 0;
	for(int i = 0; i < THREADS; ++ i) {
		shared_double_t src = (shared_double_t) (base + i);
		upc_memget((double*) (dst + tot_off), src + off[i], len[i] * sizeof(double));
		tot_off += len[i];
		upc_barrier;
	} // for
} // my_gather()


// asynchronous allgather
void my_all_gather_async(shared_1_double_t base, shared int off[], shared int len[],
						int dest, double * dst) {
	int tot_off = 0;
	upc_handle_t* handles = (upc_handle_t*) malloc(THREADS * sizeof(upc_handle_t));
	for(int i = 0; i < THREADS; ++ i) {
		shared_double_t src = (shared_double_t) (base + i);
		handles[i] = upc_memget_nb((double*) (dst + tot_off), src + off[i], len[i] * sizeof(double));
		tot_off += len[i];
	} // for
	for(int i = 0; i < THREADS; ++ i) {
		upc_sync(handles[i]);
	} // for
	upc_fence;
	free(handles);
} // my_gather()


// asynchronous allgather - implicit
void my_all_gather_asynci(shared_1_double_t base, shared int off[], shared int len[],
						int dest, double * dst) {
	int tot_off = 0;
	for(int i = 0; i < THREADS; ++ i) {
		shared_double_t src = (shared_double_t) (base + i);
		upc_memget_nbi((double*) (dst + tot_off), src + off[i], len[i] * sizeof(double));
		tot_off += len[i];
	} // for
	upc_synci();
	upc_fence;
} // my_gather()


// gather and put for fulfiling requests
void my_gather_and_put(shared_1_double_t base, int thread_id, int start_off, int lengths, int strides,
						int dst_off, double * src, unsigned int local_size) {
	shared_double_t dst = (shared_double_t) (base + thread_id);
	int src_off = start_off;
	while(src_off < local_size) {
		int size = lengths * sizeof(double);
		upc_memput(dst + dst_off, src + src_off, size);
		src_off += (lengths + strides);
		dst_off += lengths;
	} // while
} // my_gather_and_put()


void my_gather_and_put_async(shared_1_double_t base, int thread_id, int start_off, int lengths, int strides,
						int dst_off, double * src, unsigned int local_size) {
	shared_double_t dst = (shared_double_t) (base + thread_id);
	int src_off = start_off;
	upc_handle_t handle;
	while(src_off < local_size) {
		int size = lengths * sizeof(double);
		handle = upc_memput_nb(dst + dst_off, src + src_off, size);
		src_off += (lengths + strides);
		dst_off += lengths;
	} // while
	upc_sync(handle);
	upc_fence;
} // my_gather_and_put()


void my_gather_and_put_asynci(shared_1_double_t base, int thread_id, int start_off, int lengths, int strides,
						int dst_off, double * src, unsigned int local_size) {
	shared_double_t dst = (shared_double_t) (base + thread_id);
	int src_off = start_off;
	while(src_off < local_size) {
		int size = lengths * sizeof(double);
		upc_memput_nbi(dst + dst_off, src + src_off, size);
		src_off += (lengths + strides);
		dst_off += lengths;
	} // while
	upc_synci();
	upc_fence;
} // my_gather_and_put()


void set_requests(unsigned int local_size) {
	requests.src_offset = 0;
	requests.lengths = local_size / (8 * THREADS);
	requests.strides = local_size / (8 * THREADS);
	requests.dst_thread = MYTHREAD;
	requests.dst_offset = 0;
	request_avail = 1;
} // set_requests()


void process_requests(shared_1_double_t base, double * src, unsigned int local_size, UPCTimer *timer) {
	upctimer_resume(timer);
	my_gather_and_put(base, requests.dst_thread, requests.src_offset,
						requests.lengths, requests.strides, requests.dst_offset,
						src, local_size);
	request_avail = 0;
	upctimer_pause(timer);
} // process_requests()


void process_requests_async(shared_1_double_t base, double * src, unsigned int local_size, UPCTimer *timer) {
	upctimer_resume(timer);
	my_gather_and_put_async(base, requests.dst_thread, requests.src_offset,
						requests.lengths, requests.strides, requests.dst_offset,
						src, local_size);
	request_avail = 0;
	upctimer_pause(timer);
} // process_requests()


void process_requests_asynci(shared_1_double_t base, double * src, unsigned int local_size, UPCTimer *timer) {
	upctimer_resume(timer);
	my_gather_and_put_asynci(base, requests.dst_thread, requests.src_offset,
						requests.lengths, requests.strides, requests.dst_offset,
						src, local_size);
	request_avail = 0;
	upctimer_pause(timer);
} // process_requests()


// with 1D decomposition along N4
int bench3(unsigned int n1, unsigned int n2, unsigned int n3, unsigned int n4) {

	unsigned long int n123 = n1 * n2 * n3;
	unsigned int local_n4 = ceil(((float) n4) / THREADS);
	unsigned long int local_data_size = n123 * local_n4 * sizeof(double);	// in bytes

	if(MYTHREAD == 0) {
		printf("\n**      TOTAL MATRIX SIZE: %dx%dx%dx%d = %ld\n", n1, n2, n3, n4, n123 * n4);
		printf("** WORKING SET PER THREAD: %d MB\n", (int) ((double) local_data_size / 1024 / 1024));
		printf("**      NUMBER OF THREADS: %d\n\n", THREADS);
	} // if

	//shared_double_t inp_arr = (shared_double_t) upc_all_alloc(THREADS, local_data_size);
	shared_1_double_t arr = (shared_1_double_t) upc_all_alloc(THREADS, local_data_size);
	shared_double_t inp_arr = (shared_double_t) (arr + MYTHREAD);

	// set local data
	double *local_arr = (double *) malloc(local_data_size);
	for(int i = 0; i < n123 * local_n4; ++ i) local_arr[i] = n123 * local_n4 * MYTHREAD + i;
	double *local_ptr = (double *) (inp_arr);
	memcpy(local_ptr, local_arr, local_data_size);

	//double mysum = check_local_data(local_arr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ THREAD %d :: SUM = %.2f\n", MYTHREAD, mysum);

	UPCTimer ablock, bblock, cblock;
	upctimer_start(&ablock); upctimer_pause(&ablock);
	upctimer_start(&bblock); upctimer_pause(&bblock);
	upctimer_start(&cblock); upctimer_pause(&cblock);

	UPCTimer gather, gather_async, gather_asynci, allgather, allgather_async, allgather_asynci;
	upctimer_start(&gather); upctimer_pause(&gather);
	upctimer_start(&gather_async); upctimer_pause(&gather_async);
	upctimer_start(&gather_asynci); upctimer_pause(&gather_asynci);
	upctimer_start(&allgather); upctimer_pause(&allgather);
	upctimer_start(&allgather_async); upctimer_pause(&allgather_async);
	upctimer_start(&allgather_asynci); upctimer_pause(&allgather_asynci);

	upc_barrier;

	// writes

	for(int t = 0; t < NTRIALS; ++ t) {
	
		if(MYTHREAD == 0) printf("** Trial %d ...\n", t);

		// A: block write using local pointer
		upc_barrier;
		upctimer_resume(&ablock);
		memcpy(local_ptr, local_arr, local_data_size);
		upctimer_pause(&ablock);

		// B: block write using shared pointer to local
		upc_barrier;
		upctimer_resume(&bblock);
		upc_memput(inp_arr, local_arr, local_data_size);
		upctimer_pause(&bblock);

		// C: block write using shared pointer to remote (neighbor)
		upc_barrier;
		upctimer_resume(&cblock);
		upc_memput((shared_double_t) (arr + ((MYTHREAD + 1) % THREADS)), local_arr, local_data_size);
		upctimer_pause(&cblock);


		// GATHER:

		// set requests
		offsets[MYTHREAD] = 0;
		lengths[MYTHREAD] = local_n4 * n123 / THREADS;

		upctimer_resume(&gather);
		my_gather(arr, offsets, lengths, 0, local_arr);
		upctimer_pause(&gather);

		// GATHER_ASYNC:

		offsets[MYTHREAD] = local_n4 * n123 / THREADS;
		lengths[MYTHREAD] = local_n4 * n123 / THREADS;

		upctimer_resume(&gather_async);
		my_gather_async(arr, offsets, lengths, 0, local_arr);
		upctimer_pause(&gather_async);

		// GATHER_ASYNCI:

		upctimer_resume(&gather_asynci);
		my_gather_asynci(arr, offsets, lengths, 0, local_arr);
		upctimer_pause(&gather_asynci);


		// ALL GATHER:

		offsets[MYTHREAD] = MYTHREAD * local_n4 * n123 / THREADS;
		lengths[MYTHREAD] = local_n4 * n123 / THREADS;

		upctimer_resume(&allgather);
		my_all_gather(arr, offsets, lengths, 0, local_arr);
		upctimer_pause(&allgather);

		// ALL GATHER ASYNC:

		// set requests
		offsets[MYTHREAD] = ((MYTHREAD + 1) % THREADS) * local_n4 * n123 / THREADS;
		lengths[MYTHREAD] = local_n4 * n123 / THREADS;

		upctimer_resume(&allgather_async);
		my_all_gather_async(arr, offsets, lengths, 0, local_arr);
		upctimer_pause(&allgather_async);

		// ALL GATHER ASYNCI:

		upctimer_resume(&allgather_asynci);
		my_all_gather_asynci(arr, offsets, lengths, 0, local_arr);
		upctimer_pause(&allgather_asynci);

	} // for t

	upctimer_stop(&ablock);
	upctimer_stop(&bblock);
	upctimer_stop(&cblock);
	upctimer_stop(&gather);
	upctimer_stop(&gather_async);
	upctimer_stop(&gather_asynci);
	upctimer_stop(&allgather);
	upctimer_stop(&allgather_async);
	upctimer_stop(&allgather_asynci);

	/*if(MYTHREAD == 0) printf("\n** A BLOCK **\n\n");
	upc_barrier;
	print_timings(ablock, 2 * local_data_size);
	upc_barrier;
	if(MYTHREAD == 0) printf("\n** B BLOCK **\n\n");
	upc_barrier;
	print_timings(bblock, 2 * local_data_size);
	upc_barrier;
	if(MYTHREAD == 0) printf("\n** C BLOCK **\n\n");
	upc_barrier;
	print_timings(cblock, 2 * local_data_size);*/
	upc_barrier;
	if(MYTHREAD == 0) {
		printf("\n** GATHER 0 **\n\n");
		print_timings(gather, local_n4 * n123 * 2 * sizeof(double));
	} // if
	if(MYTHREAD == 0) {
		printf("\n** GATHER ASYNC 0 **\n\n");
		print_timings(gather_async, local_n4 * n123 * 2 * sizeof(double));
	} // if
	if(MYTHREAD == 0) {
		printf("\n** GATHER ASYNCI 0 **\n\n");
		print_timings(gather_asynci, local_n4 * n123 * 2 * sizeof(double));
	} // if
	if(MYTHREAD == 0) printf("\n** ALL GATHER **\n\n");
	upc_barrier;
	print_timings(allgather, local_n4 * n123 * 2 * sizeof(double));
	upc_barrier;
	if(MYTHREAD == 0) printf("\n** ALL GATHER ASYNC **\n\n");
	upc_barrier;
	print_timings(allgather_async, local_n4 * n123 * 2 * sizeof(double));
	upc_barrier;
	if(MYTHREAD == 0) printf("\n** ALL GATHER ASYNCI **\n\n");
	upc_barrier;
	print_timings(allgather_asynci, local_n4 * n123 * 2 * sizeof(double));
	upc_barrier;

	// request for data

	if(THREADS > 2)
		if(MYTHREAD == 0)
			printf("warning: this part of the code does not really support more than",
					" 2 threads yet. results obtained may be undefined\n");

	UPCTimer reqtimer; upctimer_start(&reqtimer); upctimer_pause(&reqtimer);
	UPCTimer areqtimer; upctimer_start(&areqtimer); upctimer_pause(&areqtimer);
	UPCTimer aireqtimer; upctimer_start(&aireqtimer); upctimer_pause(&aireqtimer);

	upc_lock_t* request_lock = upc_all_lock_alloc();
	if(MYTHREAD == 0) request_avail = 0;
	int num_req_per_thread = 10;

	for(int t = 0; t < NTRIALS; ++ t) {
		if(MYTHREAD == 0) printf("** Trial %d ...\n", t);
		upc_barrier;

		if(MYTHREAD == 0) { // master thread - satisfies requests from other threads
			int num_iter = num_req_per_thread * (THREADS - 1), i = 0;
			while(1) {	// keep checking and satisfying requests
				upc_lock(request_lock);
				if(request_avail == 1) {
					process_requests(arr, local_ptr, local_n4 * n123, &reqtimer);
					++ i;
				} // if
				upc_unlock(request_lock);
				if(i == num_iter) break;
			} // while
		} else {
			int num_iter = num_req_per_thread, i = 0;
			while(1) {
				upc_lock(request_lock);
				if(request_avail == 0) {
					set_requests(local_n4 * n123);
					++ i;
				} // if
				upc_unlock(request_lock);
				if(i == num_iter) break;
			} // while
		} // if
		upc_barrier;

		if(MYTHREAD == 0) { // master thread - satisfies requests from other threads
			int num_iter = num_req_per_thread * (THREADS - 1), i = 0;
			while(1) {	// keep checking and satisfying requests
				upc_lock(request_lock);
				if(request_avail == 1) {
					process_requests_async(arr, local_ptr, local_n4 * n123, &areqtimer);
					++ i;
				} // if
				upc_unlock(request_lock);
				if(i == num_iter) break;
			} // while
		} else {
			int num_iter = num_req_per_thread, i = 0;
			while(1) {
				upc_lock(request_lock);
				if(request_avail == 0) {
					set_requests(local_n4 * n123);
					++ i;
				} // if
				upc_unlock(request_lock);
				if(i == num_iter) break;
			} // while
		} // if
		upc_barrier;

		if(MYTHREAD == 0) { // master thread - satisfies requests from other threads
			int num_iter = num_req_per_thread * (THREADS - 1), i = 0;
			while(1) {	// keep checking and satisfying requests
				upc_lock(request_lock);
				if(request_avail == 1) {
					process_requests_asynci(arr, local_ptr, local_n4 * n123, &aireqtimer);
					++ i;
				} // if
				upc_unlock(request_lock);
				if(i == num_iter) break;
			} // while
		} else {
			int num_iter = num_req_per_thread, i = 0;
			while(1) {
				upc_lock(request_lock);
				if(request_avail == 0) {
					set_requests(local_n4 * n123);
					++ i;
				} // if
				upc_unlock(request_lock);
				if(i == num_iter) break;
			} // while
		} // if
	} // for

	upctimer_stop(&reqtimer);
	upctimer_stop(&areqtimer);
	upctimer_stop(&aireqtimer);

	if(MYTHREAD == 0) {
		printf("\n** Fulfilling requests **\n");
		unsigned int local_size = local_n4 * n123;
		unsigned int length = local_size / (8 * THREADS);
		unsigned int stride = local_size / (8 * THREADS);
		unsigned int data_moved = 2 * (local_size / (length + stride)) * length *
									num_req_per_thread * (THREADS - 1);
		printf("** DATA MOVED: %d MB\n", data_moved * sizeof(double) / 1024 / 1024);
		printf("\n** BLOCKING **\n");
		print_timings(reqtimer, data_moved * sizeof(double));
		printf("\n** NON-BLOCKING **\n");
		print_timings(areqtimer, data_moved * sizeof(double));
		printf("\n** NON-BLOCKING-I **\n");
		print_timings(aireqtimer, data_moved * sizeof(double));
		printf("\n");
	} // if

	upc_barrier;
	upc_all_lock_free(request_lock);

	free(local_arr);
	upc_all_free(inp_arr);
	return 0;
} // bench1()



int main(int narg, char** args) {
	unsigned int n1 = N1, n2 = N2, n3 = N3, n4 = N4;
	if(narg == 5) {
		n1 = atoi(args[1]);
		n2 = atoi(args[2]);
		n3 = atoi(args[3]);
		n4 = atoi(args[4]);
	} // if
	bench3(n1, n2, n3, n4);

	return 0;
} // main()
