/***
  *  Project:
  *
  *  File: bench4.c
  *  Created: Feb 22, 2014
  *  Modified: Wed 02 Apr 2014 10:39:54 AM PDT
  *
  *  Author: Abhinav Sarje <asarje@lbl.gov>
  */

#include <stdio.h>
#include <upc.h>
#include <upc_collective.h>
#include <upc_nb.h>
#include <bupc_atomics.h>
#include <upc_castable.h>

#include "woo_bupctimers.h"

const int N1 = 100;	// fastest dimension
const int N2 = 100;
const int N3 = 100;
const int N4 = 512;	// outermost dimension

const int NTRIALS = 5;


typedef shared [] double * shared_double_t;
typedef shared [1] double * shared_1_double_t;
typedef struct {
	unsigned int src_offset;
	unsigned int lengths;
	unsigned int strides;
	unsigned int dst_thread;
	unsigned int dst_offset;
} upc_request_t;


shared upc_request_t requests[THREADS];		// request from each thread
//shared unsigned int req_avail;				// flag	for worker -> master
shared uint64_t req_avail;						// flag	for worker -> master
shared unsigned int req_done[THREADS];			// flag for master -> worker


// for checking correctness
shared double totsum;
double check_local_data(double* arr, unsigned int size) {
	double mysum = 0.0;
	for(int i = 0; i < size; ++ i) mysum += arr[i];

	shared double * sums = (shared double *) upc_all_alloc(THREADS, sizeof(double));
	sums[MYTHREAD] = mysum;

	upc_all_reduceD(&totsum, sums, UPC_ADD, THREADS, 1, NULL, 0);
	mysum = totsum;

	upc_all_free(sums);
	return mysum / size;
} // check_local_data()


void print_timings(UPCTimer timer, double size) {
	double time = upctimer_elapsed_msec(&timer);
	printf("  ++ Thread %d:\t%.3f ms\t\t[ %.2f MB/s ]\n", MYTHREAD, time / NTRIALS,
			NTRIALS * size * 1000 / time);
} // print_results()


// gather and put for fulfiling requests
void my_gather_and_put(shared_1_double_t base, int thread_id, int start_off, int lengths, int strides,
						int dst_off, double * src, double* local_buff, unsigned int local_size) {
	int src_off = start_off;
	int buff_off = 0;
	int tot_size = 0;
	int size = lengths * sizeof(double);
	// first gather all local data
	while(src_off + lengths < local_size) {
		memcpy(local_buff + buff_off, src + src_off, size);
		src_off += (lengths + strides);
		buff_off += lengths;
		tot_size += size;
	} // while
	// and put the gathered data to its destination
	shared_double_t dst = (shared_double_t) (base + thread_id);
	if(upc_threadof(dst + dst_off) == MYTHREAD)
		memcpy((double *) dst + dst_off, local_buff, tot_size);
	else
		upc_memput(dst + dst_off, local_buff, tot_size);
} // my_gather_and_put()


void my_gather_and_put2(shared_1_double_t base, int thread_id, int start_off, int lengths, int strides,
						int dst_off, double * src, double* local_buff, unsigned int local_size) {
	int src_off = start_off;
	int buff_off = 0;
	int size = lengths * sizeof(double);
	shared_double_t dst = (shared_double_t) (base + thread_id);
	// check is pointer is local and do memcpy if it is instead
	if(upc_threadof(dst) == MYTHREAD) {
		//double* dst_ptr = upc_cast(dst);	// whats the use of this???
		double * dst_ptr = (double *) dst;
		while(src_off + lengths < local_size) {
			memcpy(dst_ptr + dst_off, src + src_off, size);
			src_off += (lengths + strides);
			dst_off += lengths;
		} // while
	} else {
		while(src_off + lengths < local_size) {
			upc_memput_nbi(dst + dst_off, src + src_off, size);
			src_off += (lengths + strides);
			dst_off += lengths;
		} // while
		upc_synci();
	} // if-else
} // my_gather_and_put2()


void set_request(unsigned int offset, unsigned int length, unsigned int stride, upc_request_t* req) {
	req->src_offset = offset;
	req->lengths = length;
	req->strides = stride;
	req->dst_thread = MYTHREAD;
	req->dst_offset = 0;
	// to notify the master
	bupc_atomicU64_fetchor_strict(&req_avail, ((uint64_t) 1) << MYTHREAD);
} // set_request()


void process_request(shared_1_double_t base, double * src, double* local_buff, unsigned int local_size,
						unsigned int tid, UPCTimer * timer) {
	// get the request into a local var
	upc_request_t req;
	upc_memget(&req, requests + tid, sizeof(upc_request_t));
	// do the gather and put
	upctimer_resume(timer);
	my_gather_and_put(base, req.dst_thread, req.src_offset,
						req.lengths, req.strides, req.dst_offset,
						src, local_buff, local_size);
	// to notify worker that its done
	bupc_atomicUI_set_strict((req_done + tid), 1);
	upctimer_pause(timer);
} // process_request()


void process_request2(shared_1_double_t base, double * src, double* local_buff, unsigned int local_size,
						unsigned int tid, UPCTimer * timer) {
	// get the request into a local var
	upc_request_t req;
	upc_memget(&req, requests + tid, sizeof(upc_request_t));
	// do the gather and put
	upctimer_resume(timer);
	my_gather_and_put2(base, req.dst_thread, req.src_offset,
						req.lengths, req.strides, req.dst_offset,
						src, local_buff, local_size);
	// to notify worker that its done
	bupc_atomicUI_set_strict((req_done + tid), 1);
	upctimer_pause(timer);
} // process_request2()


int bench4(unsigned int n1, unsigned int n2, unsigned int n3, unsigned int n4,
			unsigned int length, unsigned int stride) {

	unsigned long int n123 = n1 * n2 * n3;
	unsigned int local_n4 = ceil(((float) n4) / THREADS);
	unsigned long int local_size = n123 * local_n4;
	unsigned long int local_data_size = local_size * sizeof(double);	// in bytes

	if(MYTHREAD == 0) {
		printf("\n**      TOTAL MATRIX SIZE: %dx%dx%dx%d = %ld\n", n1, n2, n3, n4, n123 * n4);
		printf("** WORKING SET PER THREAD: %d MB\n", (int) ((double) local_data_size / 1024 / 1024));
		printf("**      NUMBER OF THREADS: %d\n\n", THREADS);
	} // if

	shared_1_double_t arr = (shared_1_double_t) upc_all_alloc(THREADS, local_data_size);
	shared_double_t inp_arr = (shared_double_t) (arr + MYTHREAD);

	// local pointers
	double *local_ptr = (double *) (inp_arr);
	unsigned int * req_done_ptr = (unsigned int *) (req_done + MYTHREAD);	// my flag
	upc_request_t * requests_ptr = (upc_request_t *) (requests + MYTHREAD);	// my request
	uint64_t * req_avail_ptr = NULL;
	if(MYTHREAD == 0) req_avail_ptr = (uint64_t *) &req_avail;			// only for master

	// allocate and set local buffer
	shared_double_t local_buff = (shared_double_t) upc_alloc(local_data_size);
	double * local_buff_ptr = (double *) local_buff;
	for(int i = 0; i < local_size; ++ i) local_buff_ptr[i] = local_n4 * MYTHREAD + i;
	// and touch your part of the shared buffer
	memcpy(local_ptr, local_buff_ptr, local_data_size);

	//double mysum = check_local_data(local_buff_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ THREAD %d :: SUM = %.2f\n", MYTHREAD, mysum);

	if(THREADS > 32)
		if(MYTHREAD == 0)
			printf("warning: this code does not support more than 32 threads yet\n");

	if(MYTHREAD == 0) { *req_avail_ptr = 0; }
	int num_req_per_thread = 20;

//	for(unsigned int stride = 1; stride <= local_size; stride *= 2) {
//	for(unsigned int length = 1; length <= local_size; length *= 2) {

	UPCTimer reqtimer; upctimer_start(&reqtimer); upctimer_pause(&reqtimer);
	UPCTimer req2timer; upctimer_start(&req2timer); upctimer_pause(&req2timer);

	if(MYTHREAD == 0) printf("\n** Gather and put: Length = %d, Stride = %d **\n", length, stride);

	// TYPE 1
	if(MYTHREAD == 0) printf("** Type 1:\n");
	for(int t = 0; t < NTRIALS; ++ t) {
		if(MYTHREAD == 0) printf("  -- Trial %d ...\n", t);
		*req_done_ptr = 1;
		upc_barrier;

		if(MYTHREAD == 0) { // MASTER thread - satisfies requests from other threads
			int num_iter = num_req_per_thread * (THREADS - 1);
			for(int i = 0; i < num_iter;) {
				unsigned int tids;
				while((tids = bupc_atomicU64_swap_private(req_avail_ptr, 0)) == 0);
				unsigned int tid = THREADS - 1;
				while(tid > 0) {	// thread 0 is the master
					// decode tids
					if(tids & (1 << tid) == 0) continue;	// WARNING: cannot have more than 32 threads!
					process_request(arr, local_ptr, local_buff_ptr, local_size, tid, &reqtimer);
					-- tid;
					++ i;
				} // while
			} // for
		} else {			// WORKER thread - generates requests for master
			int num_iter = num_req_per_thread;
			for(int i = 0; i < num_iter; ++ i) {
				// wait for your request to be done
				while(bupc_atomicUI_swap_private(req_done_ptr, 0) == 0);
				// and then set request
				set_request(0, length, stride, requests_ptr);
			} // for
		} // if
	} // for

	// TYPE 2
	if(MYTHREAD == 0) printf("** Type 2:\n");
	for(int t = 0; t < NTRIALS; ++ t) {
		if(MYTHREAD == 0) printf("  -- Trial %d ...\n", t);
		*req_done_ptr = 1;
		upc_barrier;

		if(MYTHREAD == 0) { // MASTER thread - satisfies requests from other threads
			int num_iter = num_req_per_thread * (THREADS - 1);
			for(int i = 0; i < num_iter;) {
				unsigned int tids;
				while((tids = bupc_atomicU64_swap_private(req_avail_ptr, 0)) == 0);
				unsigned int tid = THREADS - 1;
				while(tid > 0) {	// thread 0 is the master
					// decode tids
					if(tids & (1 << tid) == 0) continue;	// WARNING: cannot have more than 32 threads!
					process_request2(arr, local_ptr, local_buff_ptr, local_size, tid, &req2timer);
					-- tid;
					++ i;
				} // while
			} // for
		} else {			// WORKER thread - generates requests for master
			int num_iter = num_req_per_thread;
			for(int i = 0; i < num_iter; ++ i) {
				// wait for your request to be done
				while(bupc_atomicUI_swap_private(req_done_ptr, 0) == 0);
				// and then set request
				set_request(0, length, stride, requests_ptr);
			} // for
		} // if
	} // for

	upctimer_stop(&reqtimer);
	upctimer_stop(&req2timer);

	if(MYTHREAD == 0) {
		double data_moved = 2.0 * ((local_size / (length + stride)) * length) *
							num_req_per_thread * (THREADS - 1) * sizeof(double);
		printf("\n++ Actual gather-and-put performance **\n");
		printf("++ Type 1 data moved: %.0f MB\n", 2 * data_moved / 1024 / 1024);
		print_timings(reqtimer, 2 * data_moved / 1024 / 1024);
		printf("++ Type 2 data moved: %.0f MB\n", data_moved / 1024 / 1024);
		print_timings(req2timer, data_moved / 1024 / 1024);
		printf("\n++ Effective gather-and-put performance **\n");
		printf("++ Type 1 data moved: %.0f MB\n", data_moved / 1024 / 1024);
		print_timings(reqtimer, data_moved / 1024 / 1024);
		printf("++ Type 2 data moved: %.0f MB\n", data_moved / 1024 / 1024);
		print_timings(req2timer, data_moved / 1024 / 1024);
		printf("\n");
	} // if

	upc_barrier;
//	} // for length
//	} // for stride

	upc_free(local_buff);
	upc_all_free(inp_arr);
	return 0;
} // bench4()


int main(int narg, char** args) {
	unsigned int n1 = N1, n2 = N2, n3 = N3, n4 = N4;
	unsigned int length = 1, stride = 1;
	if(narg == 7) {
		n1 = atoi(args[1]);
		n2 = atoi(args[2]);
		n3 = atoi(args[3]);
		n4 = atoi(args[4]);
		length = atoi(args[5]);
		stride = atoi(args[6]);
	} // if
	bench4(n1, n2, n3, n4, length, stride);

	return 0;
} // main()
