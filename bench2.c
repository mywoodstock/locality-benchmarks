/***
  *  Project:
  *
  *  File: bench1.c
  *  Created: Feb 22, 2014
  *  Modified: Thu 06 Mar 2014 09:36:40 AM PST
  *
  *  Author: Abhinav Sarje <asarje@lbl.gov>
  */

#include <stdio.h>
#include <upc.h>
#include <upc_collective.h>

#include "woo_bupctimers.h"

const int N1 = 100;	// fastest dimension
const int N2 = 100;
const int N3 = 100;
const int N4 = 512;	// outermost dimension

const int NTRIALS = 5;


typedef shared [] double * shared_double_t;

shared double totsum;

double check_local_data(double* arr, unsigned int size) {
	double mysum = 0.0;
	for(int i = 0; i < size; ++ i) mysum += arr[i];

	shared double * sums = (shared double *) upc_all_alloc(THREADS, sizeof(double));
	sums[MYTHREAD] = mysum;

	upc_all_reduceD(&totsum, sums, UPC_ADD, THREADS, 1, NULL, 0);
	mysum = totsum;

	upc_all_free(sums);
	return mysum / size;
} // check_local_data()


void print_timings(UPCTimer timer, unsigned long int size) {
	upc_barrier;
	double time = upctimer_elapsed_msec(&timer);
	printf("  ++ Thread %d:\t%.3f ms\t\t[ %.2f MB/s ]\n", MYTHREAD, time / NTRIALS,
			NTRIALS * ((double) size / 1024 / 1024) * 1000 / time);
	upc_barrier;
} // print_results()


// with 1D decomposition along N4
int bench2(unsigned int n1, unsigned int n2, unsigned int n3, unsigned int n4) {

	unsigned long int n123 = n1 * n2 * n3;
	unsigned int local_n4 = ceil(((float) n4) / THREADS);
	unsigned long int local_data_size = n123 * local_n4 * sizeof(double);	// in bytes

	if(MYTHREAD == 0) {
		printf("\n**      TOTAL MATRIX SIZE: %dx%dx%dx%d = %ld\n", n1, n2, n3, n4, n123 * n4);
		printf("** WORKING SET PER THREAD: %d MB\n", (int) ((double) local_data_size / 1024 / 1024));
		printf("**      NUMBER OF THREADS: %d\n\n", THREADS);
	} // if

	//shared_double_t inp_arr = (shared_double_t) upc_all_alloc(THREADS, local_data_size);
	shared [1] double * arr = (shared [1] double *) upc_all_alloc(THREADS, local_data_size);
	shared_double_t inp_arr = (shared_double_t) (arr + MYTHREAD);

	/*printf("THREAD %d :: owner of %p is %d\n", MYTHREAD, inp_arr, upc_threadof(inp_arr));
	for(int i = 0; i < THREADS; ++ i)
		printf("THREAD %d :: but owner of %p is %d\n", MYTHREAD, arr + i, upc_threadof(arr + i));

	for(int i = 0; i < n123 * local_n4; ++ i) inp_arr[i] = MYTHREAD;
	for(int p = 0; p < THREADS; ++ p) {
		upc_barrier;
		if(MYTHREAD == p)
			for(int i = 0; i < n123 * local_n4; ++ i)
				printf("%p -> %d = %lf\n", &inp_arr[i], upc_threadof(&inp_arr[i]), inp_arr[i]);
	} // for*/

	// set local data
	double *local_arr = (double *) malloc(local_data_size);
	for(int i = 0; i < n123 * local_n4; ++ i) local_arr[i] = n123 * local_n4 * MYTHREAD + i;
	double mysum = check_local_data(local_arr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ THREAD %d :: SUM = %.2f\n", MYTHREAD, mysum);

	UPCTimer ablock, bblock, cblock;
	upctimer_start(&ablock); upctimer_pause(&ablock);
	upctimer_start(&bblock); upctimer_pause(&bblock);
	upctimer_start(&cblock); upctimer_pause(&cblock);
	UPCTimer akji, aikj, ajik, ajki, aijk, akij;
	upctimer_start(&akji); upctimer_pause(&akji);
	upctimer_start(&aikj); upctimer_pause(&aikj);
	upctimer_start(&ajik); upctimer_pause(&ajik);
	upctimer_start(&ajki); upctimer_pause(&ajki);
	upctimer_start(&aijk); upctimer_pause(&aijk);
	upctimer_start(&akij); upctimer_pause(&akij);
	UPCTimer bkji, bikj, bjik, bjki, bijk, bkij;
	upctimer_start(&bkji); upctimer_pause(&bkji);
	upctimer_start(&bikj); upctimer_pause(&bikj);
	upctimer_start(&bjik); upctimer_pause(&bjik);
	upctimer_start(&bjki); upctimer_pause(&bjki);
	upctimer_start(&bijk); upctimer_pause(&bijk);
	upctimer_start(&bkij); upctimer_pause(&bkij);
	UPCTimer ckji, cikj, cjik, cjki, cijk, ckij;
	upctimer_start(&ckji); upctimer_pause(&ckji);
	upctimer_start(&cikj); upctimer_pause(&cikj);
	upctimer_start(&cjik); upctimer_pause(&cjik);
	upctimer_start(&cjki); upctimer_pause(&cjki);
	upctimer_start(&cijk); upctimer_pause(&cijk);
	upctimer_start(&ckij); upctimer_pause(&ckij);

	upc_barrier;

	// writes

	for(int t = 0; t < NTRIALS; ++ t) {
	
	if(MYTHREAD == 0) printf("** Trial %d ...\n", t);

	// A: block write using local pointer
	upc_barrier;
	upctimer_resume(&ablock);
	double *local_ptr = (double *) (inp_arr);
	memcpy(local_ptr, local_arr, local_data_size);
	upctimer_pause(&ablock);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM = %.2f\n", mysum);

	// B: block write using shared pointer to local
	upc_barrier;
	upctimer_resume(&bblock);
	upc_memput(inp_arr, local_arr, local_data_size);
	upctimer_pause(&bblock);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM = %.2f\n", mysum);

	// C: block write using shared pointer to remote (neighbor)
	upc_barrier;
	upctimer_resume(&cblock);
	upc_memput((shared_double_t) (arr + ((MYTHREAD + 1) % THREADS)), local_arr, local_data_size);
	upctimer_pause(&cblock);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM = %.2f\n", mysum);


	// ** KJI

	// A: individual writes using local pointer
	upc_barrier;
	upctimer_resume(&akji);
	for(int l = 0; l < local_n4; ++ l) {
		for(int k = 0; k < n3; ++ k) {
			for(int j = 0; j < n2; ++ j) {
				for(int i = 0; i < n1; ++ i) {
					unsigned int local_index = l * n123 + k * n2 * n1 + j * n1 + i;
					local_ptr[local_index] = local_arr[local_index];
				} // for i
			} // for j
		} // for k
	} // for l
	upctimer_pause(&akji);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM KJI = %.2f\n", mysum);

	// B: individual writes using shared pointer to local
	upc_barrier;
	upctimer_resume(&bkji);
	for(int l = 0; l < local_n4; ++ l) {
		for(int k = 0; k < n3; ++ k) {
			for(int j = 0; j < n2; ++ j) {
				for(int i = 0; i < n1; ++ i) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput(inp_arr + index, local_arr + index, sizeof(double));
				} // for i
			} // for j
		} // for k
	} // for l
	upctimer_pause(&bkji);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM KJI = %.2f\n", mysum);

	// C: individual writes using shared pointer to neighbor (remote)
	upc_barrier;
	upctimer_resume(&ckji);
	for(int l = 0; l < local_n4; ++ l) {
		for(int k = 0; k < n3; ++ k) {
			for(int j = 0; j < n2; ++ j) {
				for(int i = 0; i < n1; ++ i) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput((shared_double_t)(arr + ((MYTHREAD + 1) % THREADS)) + index,
								local_arr + index, sizeof(double));
				} // for i
			} // for j
		} // for k
	} // for l
	upctimer_pause(&ckji);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM KJI = %.2f\n", mysum);

	// ** IKJ

	// A: individual writes using local pointer
	upc_barrier;
	upctimer_resume(&aikj);
	for(int l = 0; l < local_n4; ++ l) {
		for(int i = 0; i < n1; ++ i) {
			for(int k = 0; k < n3; ++ k) {
				for(int j = 0; j < n2; ++ j) {
					unsigned int local_index = l * n123 + k * n2 * n1 + j * n1 + i;
					local_ptr[local_index] = local_arr[local_index];
				} // for j
			} // for k
		} // for i
	} // for l
	upctimer_pause(&aikj);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM IKJ = %.2f\n", mysum);

	// B: individual writes using shared pointer to local
	upc_barrier;
	upctimer_resume(&bikj);
	for(int l = 0; l < local_n4; ++ l) {
		for(int i = 0; i < n1; ++ i) {
			for(int k = 0; k < n3; ++ k) {
				for(int j = 0; j < n2; ++ j) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput(inp_arr + index, local_arr + index, sizeof(double));
				} // for j
			} // for k
		} // for i
	} // for l
	upctimer_pause(&bikj);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM IKJ = %.2f\n", mysum);

	// C: individual writes using shared pointer to neighbor (remote)
	upc_barrier;
	upctimer_resume(&cikj);
	for(int l = 0; l < local_n4; ++ l) {
		for(int i = 0; i < n1; ++ i) {
			for(int k = 0; k < n3; ++ k) {
				for(int j = 0; j < n2; ++ j) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput((shared_double_t)(arr + ((MYTHREAD + 1) % THREADS)) + index,
								local_arr + index, sizeof(double));
				} // for j
			} // for k
		} // for i
	} // for l
	upctimer_pause(&cikj);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM IKJ = %.2f\n", mysum);

	// ** JKI

	// A: individual writes using local pointer
	upc_barrier;
	upctimer_resume(&ajki);
	for(int l = 0; l < local_n4; ++ l) {
		for(int j = 0; j < n2; ++ j) {
			for(int k = 0; k < n3; ++ k) {
				for(int i = 0; i < n1; ++ i) {
					unsigned int local_index = l * n123 + k * n2 * n1 + j * n1 + i;
					local_ptr[local_index] = local_arr[local_index];
				} // for i
			} // for k
		} // for j
	} // for l
	upctimer_pause(&ajki);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM JKI = %.2f\n", mysum);

	// B: individual writes using shared pointer to local
	upc_barrier;
	upctimer_resume(&bjki);
	for(int l = 0; l < local_n4; ++ l) {
		for(int j = 0; j < n2; ++ j) {
			for(int k = 0; k < n3; ++ k) {
				for(int i = 0; i < n1; ++ i) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput(inp_arr + index, local_arr + index, sizeof(double));
				} // for i
			} // for k
		} // for j
	} // for l
	upctimer_pause(&bjki);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM JKI = %.2f\n", mysum);

	// C: individual writes using shared pointer to neighbor (remote)
	upc_barrier;
	upctimer_resume(&cjki);
	for(int l = 0; l < local_n4; ++ l) {
		for(int j = 0; j < n2; ++ j) {
			for(int k = 0; k < n3; ++ k) {
				for(int i = 0; i < n1; ++ i) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput((shared_double_t)(arr + ((MYTHREAD + 1) % THREADS)) + index,
								local_arr + index, sizeof(double));
				} // for i
			} // for k
		} // for j
	} // for l
	upctimer_pause(&cjki);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM JKI = %.2f\n", mysum);

	// ** IJK

	// A: individual writes using local pointer
	upc_barrier;
	upctimer_resume(&aijk);
	for(int l = 0; l < local_n4; ++ l) {
		for(int i = 0; i < n1; ++ i) {
			for(int j = 0; j < n2; ++ j) {
				for(int k = 0; k < n3; ++ k) {
					unsigned int local_index = l * n123 + k * n2 * n1 + j * n1 + i;
					local_ptr[local_index] = local_arr[local_index];
				} // for k
			} // for j
		} // for i
	} // for l
	upctimer_pause(&aijk);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM IJK = %.2f\n", mysum);

	// B: individual writes using shared pointer to local
	upc_barrier;
	upctimer_resume(&bijk);
	for(int l = 0; l < local_n4; ++ l) {
		for(int i = 0; i < n1; ++ i) {
			for(int j = 0; j < n2; ++ j) {
				for(int k = 0; k < n3; ++ k) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput(inp_arr + index, local_arr + index, sizeof(double));
				} // for k
			} // for j
		} // for i
	} // for l
	upctimer_pause(&bijk);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM IJK = %.2f\n", mysum);

	// C: individual writes using shared pointer to neighbor (remote)
	upc_barrier;
	upctimer_resume(&cijk);
	for(int l = 0; l < local_n4; ++ l) {
		for(int i = 0; i < n1; ++ i) {
			for(int j = 0; j < n2; ++ j) {
				for(int k = 0; k < n3; ++ k) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput((shared_double_t)(arr + ((MYTHREAD + 1) % THREADS)) + index,
								local_arr + index, sizeof(double));
				} // for k
			} // for j
		} // for i
	} // for l
	upctimer_pause(&cijk);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM IJK = %.2f\n", mysum);

	// ** KIJ

	// A: individual writes using local pointer
	upc_barrier;
	upctimer_resume(&akij);
	for(int l = 0; l < local_n4; ++ l) {
		for(int k = 0; k < n3; ++ k) {
			for(int i = 0; i < n1; ++ i) {
				for(int j = 0; j < n2; ++ j) {
					unsigned int local_index = l * n123 + k * n2 * n1 + j * n1 + i;
					local_ptr[local_index] = local_arr[local_index];
				} // for j
			} // for i
		} // for k
	} // for l
	upctimer_pause(&akij);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM KIJ = %.2f\n", mysum);

	// B: individual writes using shared pointer to local
	upc_barrier;
	upctimer_resume(&bkij);
	for(int l = 0; l < local_n4; ++ l) {
		for(int k = 0; k < n3; ++ k) {
			for(int i = 0; i < n1; ++ i) {
				for(int j = 0; j < n2; ++ j) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput(inp_arr + index, local_arr + index, sizeof(double));
				} // for j
			} // for i
		} // for k
	} // for l
	upctimer_pause(&bkij);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM KIJ = %.2f\n", mysum);

	// C: individual writes using shared pointer to neighbor (remote)
	upc_barrier;
	upctimer_resume(&ckij);
	for(int l = 0; l < local_n4; ++ l) {
		for(int k = 0; k < n3; ++ k) {
			for(int i = 0; i < n1; ++ i) {
				for(int j = 0; j < n2; ++ j) {
					unsigned int index = l * n123 + k * n2 * n1 + j * n1 + i;
					upc_memput((shared_double_t)(arr + ((MYTHREAD + 1) % THREADS)) + index,
								local_arr + index, sizeof(double));
				} // for j
			} // for i
		} // for k
	} // for l
	upctimer_pause(&ckij);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM KIJ = %.2f\n", mysum);

	// ** JIK

	// A: individual writes using local pointer
	upc_barrier;
	upctimer_resume(&ajik);
	for(int l = 0; l < local_n4; ++ l) {
		for(int j = 0; j < n2; ++ j) {
			for(int i = 0; i < n1; ++ i) {
				for(int k = 0; k < n3; ++ k) {
					unsigned int local_index = l * n123 + k * n2 * n1 + j * n1 + i;
					local_ptr[local_index] = local_arr[local_index];
				} // for k
			} // for i
		} // for j
	} // for l
	upctimer_pause(&ajik);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM KIJ = %.2f\n", mysum);

	// B: individual writes using local pointer
	upc_barrier;
	upctimer_resume(&bjik);
	for(int l = 0; l < local_n4; ++ l) {
		for(int j = 0; j < n2; ++ j) {
			for(int i = 0; i < n1; ++ i) {
				for(int k = 0; k < n3; ++ k) {
					unsigned int local_index = l * n123 + k * n2 * n1 + j * n1 + i;
					local_ptr[local_index] = local_arr[local_index];
				} // for k
			} // for i
		} // for j
	} // for l
	upctimer_pause(&bjik);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM KIJ = %.2f\n", mysum);

	// C: individual writes using local pointer
	upc_barrier;
	upctimer_resume(&cjik);
	for(int l = 0; l < local_n4; ++ l) {
		for(int j = 0; j < n2; ++ j) {
			for(int i = 0; i < n1; ++ i) {
				for(int k = 0; k < n3; ++ k) {
					unsigned int local_index = l * n123 + k * n2 * n1 + j * n1 + i;
					local_ptr[local_index] = local_arr[local_index];
				} // for k
			} // for i
		} // for j
	} // for l
	upctimer_pause(&cjik);
	//mysum = check_local_data(local_ptr, n123 * local_n4);
	//if(MYTHREAD == 0) printf("++ SUM KIJ = %.2f\n", mysum);

	} // for t

	upctimer_stop(&ablock);
	upctimer_stop(&bblock);
	upctimer_stop(&cblock);
	upctimer_stop(&akji);
	upctimer_stop(&aikj);
	upctimer_stop(&ajik);
	upctimer_stop(&ajki);
	upctimer_stop(&aijk);
	upctimer_stop(&akij);
	upctimer_stop(&bkji);
	upctimer_stop(&bikj);
	upctimer_stop(&bjik);
	upctimer_stop(&bjki);
	upctimer_stop(&bijk);
	upctimer_stop(&bkij);
	upctimer_stop(&ckji);
	upctimer_stop(&cikj);
	upctimer_stop(&cjik);
	upctimer_stop(&cjki);
	upctimer_stop(&cijk);
	upctimer_stop(&ckij);

	double time;
	if(MYTHREAD == 0) printf("\n** A BLOCK **\n\n");
	print_timings(ablock, local_data_size);
	if(MYTHREAD == 0) printf("\n** A KJI **\n\n");
	print_timings(akji, local_data_size);
	if(MYTHREAD == 0) printf("\n** A IKJ **\n\n");
	print_timings(aikj, local_data_size);
	if(MYTHREAD == 0) printf("\n** A JIK **\n\n");
	print_timings(ajik, local_data_size);
	if(MYTHREAD == 0) printf("\n** A JKI **\n\n");
	print_timings(ajki, local_data_size);
	if(MYTHREAD == 0) printf("\n** A IJK **\n\n");
	print_timings(aijk, local_data_size);
	if(MYTHREAD == 0) printf("\n** A KIJ **\n\n");
	print_timings(akij, local_data_size);
	if(MYTHREAD == 0) printf("\n** B BLOCK **\n\n");
	print_timings(bblock, local_data_size);
	if(MYTHREAD == 0) printf("\n** B KJI **\n\n");
	print_timings(bkji, local_data_size);
	if(MYTHREAD == 0) printf("\n** B IKJ **\n\n");
	print_timings(bikj, local_data_size);
	if(MYTHREAD == 0) printf("\n** B JIK **\n\n");
	print_timings(bjik, local_data_size);
	if(MYTHREAD == 0) printf("\n** B JKI **\n\n");
	print_timings(bjki, local_data_size);
	if(MYTHREAD == 0) printf("\n** B IJK **\n\n");
	print_timings(bijk, local_data_size);
	if(MYTHREAD == 0) printf("\n** B KIJ **\n\n");
	print_timings(bkij, local_data_size);
	if(MYTHREAD == 0) printf("\n** C BLOCK **\n\n");
	print_timings(cblock, local_data_size);
	if(MYTHREAD == 0) printf("\n** C KJI **\n\n");
	print_timings(ckji, local_data_size);
	if(MYTHREAD == 0) printf("\n** C IKJ **\n\n");
	print_timings(cikj, local_data_size);
	if(MYTHREAD == 0) printf("\n** C JIK **\n\n");
	print_timings(cjik, local_data_size);
	if(MYTHREAD == 0) printf("\n** C JKI **\n\n");
	print_timings(cjki, local_data_size);
	if(MYTHREAD == 0) printf("\n** C IJK **\n\n");
	print_timings(cijk, local_data_size);
	if(MYTHREAD == 0) printf("\n** C KIJ **\n\n");
	print_timings(ckij, local_data_size);

	free(local_arr);
	upc_all_free(inp_arr);
	return 0;
} // bench1()


int main(int narg, char** args) {
	unsigned int n1 = N1, n2 = N2, n3 = N3, n4 = N4;
	if(narg == 5) {
		n1 = atoi(args[1]);
		n2 = atoi(args[2]);
		n3 = atoi(args[3]);
		n4 = atoi(args[4]);
	} // if
	bench2(n1, n2, n3, n4);

	return 0;
} // main()
