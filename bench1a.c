/***
  *  Project:
  *
  *  File: bench1.c
  *  Created: Feb 22, 2014
  *  Modified: Tue 25 Feb 2014 02:01:17 PM PST
  *
  *  Author: Abhinav Sarje <asarje@lbl.gov>
  */

#include <upc.h>
#include <stdio.h>

#include "woo_bupctimers.h"

const int N1 = 100;	// fastest dimension
const int N2 = 100;
const int N3 = 100;
const int N4 = 512;	// outermost dimension

const int NTRIALS = 10;


void print_results(UPCTimer timer, unsigned long int size) {
	upc_barrier;
	double time = upctimer_elapsed_msec(&timer);
	printf("Thread %d:\t[ %.3f ms ]\t\t%.2f MB/s\n", MYTHREAD, time / NTRIALS,
			NTRIALS * ((double) size / 1024 / 1024) * 1000 / time);
	upc_barrier;
} // print_results()


// with 1D decomposition along N4
int bench1(unsigned int n1, unsigned int n2, unsigned int n3, unsigned int n4) {

	unsigned long int total_size = n1 * n2 * n3;
	unsigned int local_n4 = ceil(((float) n4) / THREADS);
	unsigned long int local_data_size = total_size * local_n4 * sizeof(double);	// in bytes

	if(MYTHREAD == 0) {
		printf("\n**      TOTAL MATRIX SIZE: %dx%dx%dx%d = %ld\n", n1, n2, n3, n4, total_size * n4);
		printf("** WORKING SET PER THREAD: %d MB\n", (int) ((double) local_data_size / 1024 / 1024));
		printf("**      NUMBER OF THREADS: %d\n\n", THREADS);
	} // if

	shared [8] double *inp_arr = (shared [8] double*) upc_all_alloc(THREADS, local_data_size);

	upc_barrier;

	upc_forall(int i = 0; i < total_size * n4; ++ i; &inp_arr[i]) inp_arr[i] = i;

	// access array in different ways
	UPCTimer lkji, likj, ljik, ljki, lijk, lkij;
	upctimer_start(&lkji); upctimer_pause(&lkji);
	upctimer_start(&likj); upctimer_pause(&likj);
	upctimer_start(&ljik); upctimer_pause(&ljik);
	upctimer_start(&ljki); upctimer_pause(&ljki);
	upctimer_start(&lijk); upctimer_pause(&lijk);
	upctimer_start(&lkij); upctimer_pause(&lkij);
	double local_sum = 0.0;

	for(int t = 0; t < NTRIALS; ++ t) {
	upc_barrier;
	local_sum = 0.0;
	upctimer_resume(&lkji);
	for(int l = 0; l < local_n4; ++ l) {
		unsigned long int idx1 = (MYTHREAD * local_n4 + l) * total_size;
		for(int k = 0; k < n3; ++ k) {
			for(int j = 0; j < n2; ++ j) {
				for(int i = 0; i < n1; ++ i) {
					unsigned long int index = idx1 + k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	upctimer_pause(&lkji);

	upc_barrier;
	local_sum = 0.0;
	upctimer_resume(&likj);
	for(int l = 0; l < local_n4; ++ l) {
		unsigned long int idx1 = (MYTHREAD * local_n4 + l) * total_size;
		for(int i = 0; i < n1; ++ i) {
			for(int k = 0; k < n3; ++ k) {
				for(int j = 0; j < n2; ++ j) {
					unsigned long int index = idx1 + k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	upctimer_pause(&likj);

	upc_barrier;
	local_sum = 0.0;
	upctimer_resume(&ljik);
	for(int l = 0; l < local_n4; ++ l) {
		unsigned long int idx1 = (MYTHREAD * local_n4 + l) * total_size;
		for(int j = 0; j < n2; ++ j) {
			for(int i = 0; i < n1; ++ i) {
				for(int k = 0; k < n3; ++ k) {
					unsigned long int index = idx1 + k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	upctimer_pause(&ljik);

	upc_barrier;
	local_sum = 0.0;
	upctimer_resume(&ljki);
	for(int l = 0; l < local_n4; ++ l) {
		unsigned long int idx1 = (MYTHREAD * local_n4 + l) * total_size;
		for(int j = 0; j < n2; ++ j) {
			for(int k = 0; k < n3; ++ k) {
				for(int i = 0; i < n1; ++ i) {
					unsigned long int index = idx1 + k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	upctimer_pause(&ljki);

	upc_barrier;
	local_sum = 0.0;
	upctimer_resume(&lijk);
	for(int l = 0; l < local_n4; ++ l) {
		unsigned long int idx1 = (MYTHREAD * local_n4 + l) * total_size;
		for(int i = 0; i < n1; ++ i) {
			for(int j = 0; j < n2; ++ j) {
				for(int k = 0; k < n3; ++ k) {
					unsigned long int index = idx1 + k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	upctimer_pause(&lijk);

	upc_barrier;
	local_sum = 0.0;
	upctimer_resume(&lkij);
	for(int l = 0; l < local_n4; ++ l) {
		unsigned long int idx1 = (MYTHREAD * local_n4 + l) * total_size;
		for(int k = 0; k < n3; ++ k) {
			for(int i = 0; i < n1; ++ i) {
				for(int j = 0; j < n2; ++ j) {
					unsigned long int index = idx1 + k * n2 * n1 + j * n1 + i;
					local_sum += inp_arr[index];
				} // for i
			} // for j
		} // for k
	} // for l
	upctimer_pause(&lkij);
	} // for t

	upctimer_stop(&lkji);
	upctimer_stop(&likj);
	upctimer_stop(&ljik);
	upctimer_stop(&ljki);
	upctimer_stop(&lijk);
	upctimer_stop(&lkij);

	if(MYTHREAD == 0) printf("\n** LKJI *****\n\n");
	print_results(lkji, local_data_size);
	if(MYTHREAD == 0) printf("\n** LIKJ *****\n\n");
	print_results(likj, local_data_size);
	if(MYTHREAD == 0) printf("\n** LJIK *****\n\n");
	print_results(ljik, local_data_size);
	if(MYTHREAD == 0) printf("\n** LJKI *****\n\n");
	print_results(ljki, local_data_size);
	if(MYTHREAD == 0) printf("\n** LIJK *****\n\n");
	print_results(lijk, local_data_size);
	if(MYTHREAD == 0) printf("\n** LKIJ *****\n\n");
	print_results(lkij, local_data_size);

	upc_all_free(inp_arr);
	return 0;
} // bench1()

int main(int narg, char** args) {
	unsigned int n1 = N1, n2 = N2, n3 = N3, n4 = N4;
	if(narg == 5) {
		n1 = atoi(args[1]);
		n2 = atoi(args[2]);
		n3 = atoi(args[3]);
		n4 = atoi(args[4]);
	} // if
	bench1(n1, n2, n3, n4);

	return 0;
} // main()
